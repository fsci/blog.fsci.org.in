# Planet FSCI

Planet URL - http://planet.fsci.in/ <br>
Planet feed URL - https://planet.fsci.in/atom.xml <br>
_Planet refreshes six hour._

Feel free to send merge requests for updates/additions. _We appreciate if the feed is full-text and not post summary. People who use feed reader would greatly appreciate getting the full text and not have to fire up a browser just to read a single post :)_

**Please mention the MR in FSCI matrix or xmpp channel, so that we don't miss it :)**

### Steps to add blog

- Fork the repository.
- Add your feed URL and name in `config.ini` file.
- Make a merge request here.

If you're unsure about Git, you can send a mail to admin AT fsci.org with your blog feed and full name to get your blog added to Planet FSCI. 

### Steps for making/testing styling changes

- Switch to Pipelines section under CI/CD in sidebar OR simply click [here](https://gitlab.com/fsci/blog.fsci.org.in/-/pipelines).
- In column next to Duration, click on Download icon for downloading the Artifacts.
- Unzip it and switch to `/public` directory.
- Make changes in `index.html`. CSS is embedded inside `<head>` tag in `<style>`. Open this (local) copy in browser to view your modifications.
- Cherry-pick your changes and add them to your fork in `index.html.tmp` file under `theme` directory. 
- (optional) If you want to confirm if CI/CD pipeline passes, push the code and visit `<username>.gitlab.io/blog.fsci.org.in` to see live results.
- Send a merge request and don't forget to squash the commits.

### Steps for making/testing structural changes

- Make modifications in `index.html.tmp` file under `theme` directory.
- To preview the changes, push the code (in your fork) and let CI/CD pipeline build. See the preview at `<username>.gitlab.io/blog.fsci.org.in`.
- Send a merge request after testing everything and don't forget to squash the commits.

### Steps to add new free software related planets in [Other Planets list](/README.md#other-planets-in-alphabetical-order) below

- Find the planet website, rss feed url (if available) and atom feed url (if available).
- Edit [README](https://gitlab.com/fsci/blog.fsci.org.in/-/blob/master/README.md) file. Add planet in alphabetical order. Follow `Name - website, rss, atom.` format. 

### Why does repository name and git/repository URL differ

Initially the planet was served under blog.fsci.org.in which was later changed to planet.fsci.in. As the planet makes use of a container registry, it doesn't allow the change in initial path/URL to planet.fsci.in. 

Here's the error message shown:
```
Cannot rename project because it contains container registry tags!
```
### Other Planets (in alphabetical order)

 List may not be exhaustive. Please file an MR if you found any missing free software planet.

- Arch - [website](https://archlinux.org/planet/), [rss](https://archlinux.org/feeds/planet/).
- Bugzilla - [website](https://planet.bugzilla.org/), [rss](https://planet.bugzilla.org/rss20.xml), [atom](https://planet.bugzilla.org/atom.xml).
- Clojure - [website](http://planet.clojure.in/), [rss](http://www.planeterlang.com/atom.xml).
- Debian - [website](https://planet.debian.org/), [rss](https://planet.debian.org/rss20.xml), [atom](https://planet.debian.org/atom.xml).
- Debian Derivatives - [website](https://planet.debian.org/deriv/), [rss](https://planet.debian.org/deriv/rss20.xml), [atom](https://planet.debian.org/deriv/atom.xml).
- dgplug - [website](http://planet.dgplug.org/), [atom](http://planet.dgplug.org/atom.xml).
- Django - [website](https://www.djangoproject.com/community/blogs/), [rss](https://www.djangoproject.com/rss/community/blogs/).
- Drupal - [website](https://www.drupal.org/planet), [rss](https://www.drupal.org/planet/rss.xml).
- Erlang - [website](http://www.planeterlang.com/), [rss](http://www.planeterlang.com/atom.xml).
- Fedora - [website](http://fedoraplanet.org/), [rss](http://fedoraplanet.org//rss20.xml), [atom](http://fedoraplanet.org//atom.xml).
- freedesktop.org - [website](https://planet.freedesktop.org/), [rss](https://planet.freedesktop.org/rss20.xml), [atom](https://planet.freedesktop.org/atom.xml).
- FSF Europe - [website](https://planet.fsfe.org/), [rss](https://planet.fsfe.org/en/rss20.xml), [atom](https://planet.fsfe.org/en/atom.xml).
- Gentoo - [website](https://planet.gentoo.org/), [rss](https://planet.gentoo.org/rss20.xml), [atom](https://planet.gentoo.org/atom.xml).
- GNOME - [website](https://planet.gnome.org/), [rss](https://planet.gnome.org/rss20.xml), [atom](https://planet.gnome.org/atom.xml).
- GNU - [website](https://planet.gnu.org/), [rss](https://planet.gnu.org/rss20.xml), [atom](https://planet.gnu.org/atom.xml).
- Haskell - [website](https://planet.haskell.org/), [rss](https://planet.haskell.org/rss20.xml), [atom](https://planet.haskell.org/atom.xml).
- ILUG Delhi - [website](https://linuxdelhi.org/planet/).
- Julia - [website](https://www.juliabloggers.com/), [rss](https://www.juliabloggers.com/feed/).
- KDE - [website](https://planet.kde.org/), [rss](https://planet.kde.org/index.xml), [atom](https://planet.kde.org/atom.xml).
- Linux Kernel - [website](https://planet.kernel.org/), [rss](https://planet.kernel.org/rss20.xml)
- Lisp - [website](https://planet.lisp.org/), [rss](https://planet.lisp.org/rss20.xml).
- Mageia - [website](https://planet.mageia.org/en/), [atom](https://planet.mageia.org/en/atom.php).
- MariaDB - [website](https://mariadb.org/planet/), [rss](https://mariadb.org/planet-rss), [atom](https://mariadb.org/planet-atom). 
- Mozilla - [website](https://planet.mozilla.org/), [rss](https://planet.mozilla.org/rss20.xml), [atom](https://planet.mozilla.org/atom.xml).
- MySQL - [website](https://planet.mysql.com/), [rss](https://planet.mysql.com/rss20.xml).
- NixOS - [website](https://planet.nixos.org/), [atom](https://planet.nixos.org/atom.xml).
- openSUSE (en) - [website](https://planet.opensuse.org/en/), [rss](https://planet.opensuse.org/global/rss20.xml) (global).
- Perl - [website](http://blogs.perl.org/), [atom](http://blogs.perl.org/atom.xml).
- PostgreSQL - [website](https://planet.postgresql.org/), [rss](https://planet.postgresql.org/rss20.xml), [rss - summary](https://planet.postgresql.org/rss20_short.xml).
- Python - [website](https://www.planetpython.org/), [rss](https://www.planetpython.org/rss20.xml).
- Scheme - [website](https://planet.scheme.org/), [atom](https://planet.scheme.org/atom.xml).
- The Document Foundation - [website](https://planet.documentfoundation.org/), [atom](https://planet.documentfoundation.org/atom.xml).
- Ubuntu - [website](https://planet.ubuntu.com/), [rss](http://planet.ubuntu.com/rss20.xml).
- Virt Tools (QEMU, KVM, etc) - [website](https://planet.virt-tools.org/), [rss](https://planet.virt-tools.org/rss20.xml), [atom](https://planet.virt-tools.org/atom.xml).
- Wordpres - [website](https://planet.wordpress.org/), [rss](https://planet.wordpress.org/feed/).

### License

The setup is licensed [GPL 3+](https://gitlab.com/fsci/blog.fsci.org.in/-/blob/master/LICENSE). The theme related files stored in `theme` directory has their own [license file](https://gitlab.com/fsci/blog.fsci.org.in/-/blob/master/theme/LICENCE).
